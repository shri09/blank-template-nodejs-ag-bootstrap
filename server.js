'use strict';

var $url = require('url');
var cors = require('cors');
var $express = require('express');
var proxyMiddleware = require('http-proxy-middleware');

require('events').EventEmitter.prototype._maxListeners = 100; // setting max listeners globally

// only load when not on cloudfoundry, but already defining var up front
var $buenosHttps;

try {
    // try to load the https module, this is only available on dev
    if (require.resolve('buenos-https')) {
        $buenosHttps = require('buenos-https');
    }
} catch (e) {}

if (!module.parent) {
    startServer();
}

function startServer () {

    var app = $express();

    app.use(function (req, res, next) {
        // header is used on f.e. cloud foundry, internal traffic is all http
        var protocol = req.headers['x-forwarded-proto'] || req.protocol;
        if (protocol !== 'https' && req.hostname !== 'localhost') {
            res.writeHead(403, 'Forbidden');
            res.end('HTTPS connection required');
        }
        else {
            next();
        }

    });
    app.use(cors());
    app.use($express.static('public'));

    if (!process.env.VCAP_APP_PORT) {
        app = $buenosHttps(app);
    }

    app.listen(process.env.VCAP_APP_PORT || 8234, function () {
        if (!process.env.VCAP_APP_PORT) {
            console.log('App running on localhost:8234');
        }
    });

}
