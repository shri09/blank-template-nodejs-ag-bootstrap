/* istanbul ignore next */(function(f){if(typeof exports==="object"&&typeof module!=="undefined"){module.exports=f()}else if(typeof define==="function"&&define.amd){define([],f)}else{var g;if(typeof window!=="undefined"){g=window}else if(typeof global!=="undefined"){g=global}else if(typeof self!=="undefined"){g=self}else{g=this}g.yes = f()}})(function(){var define,module,exports;return /* istanbul ignore next */(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
'use strict';

var $popup = require('./lib/popup');
var $redirect = require('./lib/redirect');
var $events = require('./lib/events');
var $oauth = require('./lib/oauth');
var $helper = require('./lib/helper');

var api = login;
api.subtractQueryParams = $helper.subtractQueryParams;
api.parseUrlObject = $helper.parseUrlObject;

module.exports = {
    login: api
};

var CONFIG_TEMPLATE = {
    dryRun: 'boolean',
    clientId: 'string',
    state: 'string',
    loginUrl: 'string',
    brand: 'string',
    locale: 'string',
    authFlow: {
        type: 'string',
        values: ['popup', 'redirect']
    },
    requestConfig: {
        type: 'object',
        properties: {
            host: 'string',
            pathname: 'string'
        }
    }
};

function login (options) {

    return new LoginFlow(options);

}

function LoginFlow (options) {

    var self = this;

    self.options = _parseOptions(options);

    self.eventHandler = new $events.EventHandler(self);
    self.oauth = new $oauth.Oauth(self);

    self.abort = abort;

    var url = self.options.loginUrl ||
        $helper.parseUrlObject(self.oauth.getRequestConfig());


    // popup flow or redirect flow?
    if (self.options.authFlow === 'popup') {

        self.popup = new $popup(self);

        self.api = {
            on: self.eventHandler.on,
            removeListener: self.eventHandler.removeListener,
            abort: abort
        };

        self.popup.go(url);

    }
    else {

        self.redirect = new $redirect(self);

        var redirectUrl = self.redirect.go(url);

        self.api = {
            url: redirectUrl
        };

    }

    return self.api;

    function abort (soft) {

        self.oauth.abort(soft);
        self.popup.abort(soft);

        // unsubscribe all event listeners
        for (var key in $events.events) {
            /* istanbul ignore else */
            if ($events.events.hasOwnProperty(key)) {
                self.eventHandler.removeListener(key);
            }
        }

    }

    function _parseOptions (options) {

        if (typeof options !== 'object') {
            throw 'options must be an object';
        }

        var cleanConfig = _checkTypes(CONFIG_TEMPLATE, options);

        // either a loginUrl or a clientId is required
        if (cleanConfig.loginUrl) {

            // client is using custom URL, which is ok. Just validate it's using SSL
            if (cleanConfig.loginUrl.indexOf('https:') !== 0) {
                throw 'options.loginUrl must use https';
            }

        }
        else {
            if (!cleanConfig.clientId) {
                throw 'options.clientId or options.loginUrl are required';
            }
        }

        cleanConfig.requestConfig = cleanConfig.requestConfig || {};
        cleanConfig.authFlow = cleanConfig.authFlow || 'popup';

        // if a redirect_uri is provided, make sure it's https
        cleanConfig.redirect_uri = options.redirect_uri;
        if (cleanConfig.redirect_uri && cleanConfig.redirect_uri.indexOf('https:') !== 0) {
            throw 'options.redirect_uri must use https';
        }

        return cleanConfig;

    }


    function _checkTypes (specs, config, parentProperty) {

        var cleanConfig = {};

        for (var key in specs) {
            /* istanbul ignore else */
            if (specs.hasOwnProperty(key)) {

                // store type for reference (may be undefined)
                var givenType = typeof config[key];
                var requiredType = specs[key].type || specs[key];

                // is this config option specified in config?
                if (givenType !== 'undefined') {

                    // confirm types match
                    if (requiredType === givenType) {

                        // appears valid, needs nested checking if it was an object
                        if (givenType === 'object') {

                            // call recursively
                            cleanConfig[key] = _checkTypes(specs[key].properties, config[key], [parentProperty, key].join('.'));

                        }
                        else if (requiredType === 'string' &&
                            typeof specs[key].values !== 'undefined') {

                            if (specs[key].values.indexOf(config[key]) > -1) {
                                cleanConfig[key] = config[key];
                            }
                            else {

                                throw 'Property ' + [parentProperty, key].join('.').substr(1) +
                                    ' must be one of [' + specs[key].values.join(', ') + ']';

                            }
                        }
                        else {
                            cleanConfig[key] = config[key];
                        }

                    }
                    else {

                        throw 'Property ' + [parentProperty, key].join('.').substr(1) +
                            ' should be of type ' + requiredType;

                    }

                }

            }
        }

        return cleanConfig;

    }

}

},{"./lib/events":2,"./lib/helper":3,"./lib/oauth":4,"./lib/popup":5,"./lib/redirect":6}],2:[function(require,module,exports){
'use strict';

module.exports = {
    EventHandler: EventHandler,
    events: {
        OAUTH_EVENT: 'oauthEvent',
        OAUTH_SUCCESS: 'oauthSuccess',
        OAUTH_ERROR: 'oauthError',
        OAUTH_STATE_MISMATCH: 'oauthStateMismatch',
        OAUTH_CANCELLED: 'oauthCancelled',
        OAUTH_FLOW_DETACHED: 'oauthFlowDetached'
    }
};

function EventHandler (instance) {

    var self = this;

    self.on = on;
    self.removeListener = removeListener;
    self.emit = emit;

    var boundEvents = {};

    function on (eventName, listener) {

        _checkEventName(eventName);

        boundEvents[eventName].push(listener);

        return instance;
    }

    function removeListener (eventName, listener) {

        _checkEventName(eventName);

        if (typeof listener === 'undefined') {

            // remove all listeners for event
            boundEvents[eventName].length = 0;

        }
        else {

            for (var i = 0, iMax = boundEvents[eventName].length; i < iMax; i++) {


                if (boundEvents[eventName][i] === listener) {
                    boundEvents[eventName].splice(i, 1);
                    break;
                }

            }

        }

        return instance;

    }

    function emit (eventName) {

        _checkEventName(eventName);

        // parse all arguments to an array
        var args = Array.prototype.slice.call(arguments);

        // remove eventName from array
        args.shift();

        // cache the bound functions in a new array so it cannot be manipulated
        var eventListeners = [];
        for (var i = 0, iMax = boundEvents[eventName].length; i < iMax; i++) {

            eventListeners.push(boundEvents[eventName][i]);

        }

        // call all bound listeners using a timeout for async
        for (i = 0, iMax = eventListeners.length; i < iMax; i++) {

            _emitEvent(eventListeners[i], args);

        }

        return instance;

    }

    function _emitEvent (fn, args) {

        // wrap in a timeout so it's called async, this severely increases performance
        setTimeout(function () {
            fn.apply(self.instance, args);
        }, 0);

    }

    /**
     * This utility function forces presence of an eventName for convenience
     */
    function _checkEventName (eventName) {

        boundEvents[eventName] = boundEvents[eventName] || [];

    }

}



},{}],3:[function(require,module,exports){
'use strict';

module.exports = {
    subtractQueryParams: subtractQueryParams,
    parseUrlObject: parseUrlObject
};


function subtractQueryParams (searchString) {

    var queryPartsArray = searchString.split('?');

    queryPartsArray.splice(0, 1); // delete part after ?

    var queryParts = {};
    for (var i = 0; i < queryPartsArray.length; i++) {
        queryPartsArray[i] = queryPartsArray[i].split('=');
        queryParts[decodeURIComponent(queryPartsArray[i][0])] = decodeURIComponent(queryPartsArray[i][1]);
    }

    return queryParts;

}

function parseUrlObject (urlObject) {

    var url = urlObject.protocol;
    url += urlObject.slashes ? '//' : '';
    url += urlObject.host;
    url += urlObject.pathname;

    if (urlObject.query) {
        var queryParams = [];
        for (var key in urlObject.query) {
            /* istanbul ignore else */
            if (urlObject.query.hasOwnProperty(key)) {
                queryParams.push(encodeURIComponent(key) + '=' + encodeURIComponent(urlObject.query[key]));
            }
        }

        if (queryParams.length) {
            url += '?' + queryParams.join('&');
        }
    }

    return url;

}

},{}],4:[function(require,module,exports){
'use strict';

var $events = require('./events').events;

module.exports = {
    Oauth: Oauth
};

var COOKIE_NAME = 'afklWebLogonState';

function Oauth (instance) {

    var self = this;

    self.getRequestConfig = getRequestConfig;
    self.validateState = validateState;
    self.getStateCookieValue = getStateCookieValue;
    self.abort = abort;

    // when an oauth event is triggered make sure the state token is cleared
    instance.eventHandler.on($events.OAUTH_EVENT, function () {

        // wrapped in a function, eventHandler may send arguments that trigger an undesired soft destroy
        _destroy();
    });

    // remove any cookies that are present
    _setStateCookie(false);

    function getRequestConfig () {
        var requestConfig = {
            protocol: 'https:',
            slashes: true,
            host: instance.options.requestConfig.host || 'www.klm.com',
            pathname: instance.options.requestConfig.pathname || '/oauthcust/oauth/',
            query: {
                scope: 'logon-type:afkl',
                response_type: 'code',
                client_id: instance.options.clientId,
                state: _setStateCookie(instance.options.state || _generateRandomStateToken()),
                brand: instance.options.brand || 'all',
                locale: instance.options.locale || 'gb/en'
            }
        };

        if (instance.options.redirect_uri) {
            requestConfig.query.redirect_uri = instance.options.redirect_uri;
        }

        return requestConfig;

    }

    function validateState (state) {

        var cookieState = getStateCookieValue();

        /**
         * state is valid when:
         * - cookiestate is falsy
         * - cookiestate and state argument are exactly the same
         *
         * these conditions are also validating the following situations
         * - both cookiestate and state argument are falsy
         *  (state arg does not matter if cookiestate is falsy) -> valid
         * - state arg is falsy but cookiestate is not
         *   (this implies the cookiestate is not matched though it should have been) -> invalid
         */

        return !cookieState ||
            cookieState === state;

    }

    function getStateCookieValue () {

        var cookieValue;

        var name = COOKIE_NAME + '=';
        var cookieParts = window.document.cookie.split(';');

        for (var i = 0, iMax = cookieParts.length; i < iMax; i++) {
            var cookieString = cookieParts[i];

            // trim leading whitespace from cookie part
            while (cookieString.charAt(0) === ' ') {
                cookieString = cookieString.substring(1);
            }

            if (cookieString.indexOf(name) === 0) {
                cookieValue = decodeURIComponent(cookieString.substring(name.length, cookieString.length));
                break;
            }
        }

        if (!cookieValue) {
            cookieValue = undefined;
        }

        return cookieValue;

    }

    function abort (soft) {

        _destroy(soft);

    }

    function _generateRandomStateToken () {

        return new Date().getTime() * Math.random() * 1000;

    }

    function _setStateCookie (state) {

        var date = new Date();

        if (state) {
            // cookie is valid for 15 minutes
            date.setTime(date.getTime() + 15 * 60 * 1000);
        }
        else {
            // when no state, remove cookie by setting validity to expired
            state = 'EXPIRED';
            date.setTime(date.getTime() - 30 * 24 * 60 * 60 * 1000);
        }

        var cookieParams = [
            COOKIE_NAME + '=' + encodeURIComponent(state),
            'expires=' + date.toUTCString(),
            'path=/',
            'secure'
        ];

        window.document.cookie = cookieParams.join(';');

        return state;

    }

    function _destroy (soft) {

        if (!soft) {
            _setStateCookie(false);
        }
        instance.eventHandler.removeListener($events.OAUTH_EVENT, _destroy);

    }
}


},{"./events":2}],5:[function(require,module,exports){
'use strict';

var $events = require('./events').events;
var $helper = require('./helper');

module.exports = Popup;

var activePopupId;

function Popup (instance) {

    var self = this;

    self.go = go;
    self.abort = abort;

    var popupWindow;
    var popupWindowInterval;
    var instancePopupId = new Date().getTime().toString();

    function go (url) {

        var windowId = 'afklWeblogon' + (instance.options.clientId || '');
        var popupConfig = _getPopupConfig();

        popupWindow = window.open(url, windowId, popupConfig);

        _setPopupId(instancePopupId);

        popupWindowInterval = setInterval(_watch, 100);

        // when an oauth event is triggered make sure the popup interval is removed and the window is closed
        instance.eventHandler.on($events.OAUTH_EVENT, function () {

            // wrapped in a function, eventHandler may send arguments that trigger an undesired soft destroy
            _destroy();
        });

        instance.eventHandler.on($events.OAUTH_FLOW_DETACHED, function () {

            // perform a soft abort
            instance.abort(true);

        });

        /* istanbul ignore else */
        if (window.focus) {
            popupWindow.focus();
        }
    }

    function abort (soft) {

        _destroy(soft);

    }

    function _closePopup () {

        popupWindow.close();

    }

    function _watch () {


        // need to wrap in try/catch,
        // accessing the popup while it is on the auth serve domain throws a cross domain security error
        try {
            if (!popupWindow || popupWindow.closed) {
                // window is gone or marked as closed, probably cancelled by closing the window

                _emitEvent($events.OAUTH_EVENT);
                _emitEvent($events.OAUTH_CANCELLED);

            }
            else if (instancePopupId !== activePopupId) {

                _emitEvent($events.OAUTH_FLOW_DETACHED);
            }
            else if (popupWindow.document.readyState === 'complete' &&
                (popupWindow.location.hash.indexOf('code=') > -1 ||
                popupWindow.location.hash.indexOf('error=') > -1)) {


                clearInterval(popupWindowInterval); // no need to check for events once we get error or auth code back

                // window location is accessible and has a query param that's interesting

                // parse window location query params into an object

                var queryParams = $helper.subtractQueryParams(popupWindow.location.hash);

                if (queryParams.error) {

                    // appears to be bad, still need to validate state though

                    _parseOauthResult($events.OAUTH_ERROR, queryParams);

                }
                else if (queryParams.code) {

                    // appears to be ok but might still need to validate the state

                    _parseOauthResult($events.OAUTH_SUCCESS, queryParams);

                }
                else {
                    // someting went wrong in getting response parameter
                    _emitEvent($events.OAUTH_EVENT);
                    _emitEvent($events.OAUTH_FLOW_DETACHED);
                }

            }

        }
        catch (e) {}

    }

    function _parseOauthResult (oauthEventType, queryParams) {

        // TODO: Check why state value not coming back after succesful login, and once done remove first condition from if condition

        // if state is present, or a cookie state is present, the state MUST match
        if (queryParams.state && !instance.oauth.validateState(queryParams.state)) {

            // state mismatch! not good
            // if it doesn't match there may have been some tampering, emit the state mismatch event instad
            // of the provided event
            oauthEventType = $events.OAUTH_STATE_MISMATCH;

        }

        // emit the event
        _emitEvent(oauthEventType, queryParams);

        // and always publish this event for state-agnostic listeners
        _emitEvent($events.OAUTH_EVENT, oauthEventType, queryParams);

    }

    function _destroy (soft) {

        clearInterval(popupWindowInterval);

        if (!soft) {
            _setPopupId(false);
            _closePopup();
        }

    }

    function _emitEvent () {

        instance.eventHandler.emit.apply(null, arguments);

    }
}


function _getPopupConfig () {

    var popupWindowSpecs = {
        height: 500,
        width: 763,
        menubar: 'no',
        resizable: 'no',
        scrollbars: 'no',
        status: 'no',
        toolbar: 'no'
    };

    var dualScreenLeft = window.screenLeft || screen.left || 0;
    var dualScreenTop = window.screenTop || screen.top || 0;

    var browserWidth = window.innerWidth || document.documentElement.clientWidth || screen.width;
    var browserHeight = window.innerHeight || document.documentElement.clientHeight || screen.height;

    popupWindowSpecs.left = browserWidth / 2 - popupWindowSpecs.width / 2 + dualScreenLeft;
    popupWindowSpecs.top = browserHeight / 2 - popupWindowSpecs.height / 2 + dualScreenTop;

    var parsedSpecs = [];
    for (var key in popupWindowSpecs) {
        /* istanbul ignore else */
        if (popupWindowSpecs.hasOwnProperty(key)) {
            parsedSpecs.push(key + '=' + popupWindowSpecs[key]);
        }
    }
    parsedSpecs = parsedSpecs.join(',');

    return parsedSpecs;

}

function _setPopupId (id) {

    if (id) {
        activePopupId = id;
    }
    else {
        activePopupId = undefined;
    }

}

},{"./events":2,"./helper":3}],6:[function(require,module,exports){
'use strict';

module.exports = Redirect;

function Redirect (instance) {

    var self = this;

    self.go = go;

    function go (url) {

        if (instance.options.dryRun === true) {
            url = '#' + url;
        }

        (location || document.location || window.document.location).href = url;

        return url;

    }

}

},{}],7:[function(require,module,exports){
'use strict';

// assign to window
window.afklWebLogon = require('../afkl-weblogon');

},{"../afkl-weblogon":1}]},{},[7])(7)
});